﻿using System;
using System.Collections.Generic;

public class Duck : Animal
{
    public Duck(string name, List<string> actions   ) : base(name,actions)
    {
        this.sound = "quac";
    }
}
