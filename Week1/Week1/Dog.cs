﻿using System;
using System.Collections.Generic;

public class Dog : Animal
{
	public Dog(string name, List<string> actions) : base(name,actions)
	{
        this.sound = "woof";
	}
}
