﻿using System;
using System.Collections.Generic;

public class Alpaca : Animal
{
    public Alpaca(string name, List<string> actions) : base(name,actions)
    {
        this.sound = "arrrgh";
    }
}
