﻿using System;
using System.Collections.Generic;

namespace Week1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> listOfAnimals = new List<Animal>();
            listOfAnimals.Add(new Dog("Rex", new List<string> { "merge","invata","scrie" }));
            listOfAnimals.Add(new Dog("Azorel",new List<string> { "invata2", "scrie2" }));
            listOfAnimals.Add(new Dog("Iancu",new List<string> { "smth" }));
            listOfAnimals.Add(new Cat("Max",new List<string> { "doarme", "mananca", "se joaca" }));
            foreach (Animal animal in listOfAnimals)
            {
                animal.MakeSound();
            }
            foreach(Animal animal in listOfAnimals)
            {
                animal.MakeAction();
            }
        }
    }
}
