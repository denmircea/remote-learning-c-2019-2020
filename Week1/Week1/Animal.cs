﻿using System;
using System.Collections.Generic;

public abstract class Animal
{
    protected string name;
    protected string sound;
    protected List<string> actions;
    public Animal(string name,List<string> actions) 
    {
        this.name = name;
        this.actions = actions;
    }
    public void MakeSound ()
    {
        Console.WriteLine(  this.name + " makes " + this.sound);
        return;
    }
    public void MakeAction()
    {
        foreach(var action in actions)
        {
            Console.WriteLine(this.name + " " + action);
        }
    }
}
