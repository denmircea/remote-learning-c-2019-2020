﻿using System;
using System.Collections.Generic;

public class Cat : Animal
{
    public Cat(string name,List<string> actions) : base(name,actions)
    {
        this.sound = "miau";
    }
}
